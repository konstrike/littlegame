package com.company.UI;

import com.company.Entities.Items.Item;
import com.company.Repository.ItemRepo;
import com.company.Services.MobFightService;
import com.company.Entities.Player;
import com.company.Services.PlayerService;
import com.company.Services.TradeBuyService;

import java.util.Map;
import java.util.Scanner;

public class BaseUI {
    private Player player;
    private ItemRepo repo;
    private MobFightService mfs;
    private PlayerService ps;
    private TradeBuyService tbs;

    public BaseUI(Player player, ItemRepo repo, MobFightService mfs, PlayerService ps, TradeBuyService tbs) {

        this.player = player;
        this.repo = repo;
        this.mfs = mfs;
        this.ps = ps;
        this.tbs = tbs;
    }

    private void displayStats() {
        System.out.println("Player Stats:");
        "Player Stats:".chars().forEach(i -> System.out.print("-"));
        System.out.println("\n" + player.toString());
    }

    private void displayInventory() {
        System.out.println("Inventoty:");
        for (Item elem : repo.getAll()) {
            System.out.println("--------");
            System.out.println(elem.toString());
        }
        System.out.println("--------");
    }

    private void displayMenu() {
        System.out.println("\n1)Display stats\n" +
                "2)Inventory\n" +
                "3)Rest\n" +
                "4)Fight\n" +
                "5)Trade\n" +
                "6)Soul Count\n" +
                "0)Exit\n");
    }

    private void tradeMenudisplay() {
        System.out.println("1)Upgrade\n" +
                "2)Buy\n" +
                "0)Back");
    }

    private void tradeMenuUpgradeDisplay() {
        System.out.println("1)Upgrade Damage (10 Red Shards)\n" +
                "2)Upgrade Health (10 Blue Shards)\n" +
                "0)Exit");
    }

    private void displaySoulCount()
    {
        Map<String,Integer> soulCount=player.getSoulCount();
        for(String ranks:soulCount.keySet())
        {
            System.out.println(ranks+":"+soulCount.get(ranks));
        }
    }

    private void tradeMenu() {
        Scanner sc = new Scanner(System.in);
        while (1 == 1) {
            System.out.println("Shady Merchant:");
            System.out.println("-What do you want?!");
            tradeMenudisplay();
            int option = Integer.parseInt(sc.nextLine());
            switch (option) {
                case 1: {
                    System.out.println("Shady Merchant:");
                    System.out.println("-I hope you have enough shards.");
                    tradeMenuUpgradeDisplay();
                    int option1 = Integer.parseInt(sc.nextLine());
                    try {
                        tbs.upgrade(option1);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                }
                case 0: {
                    System.out.println("Shady Merchant:");
                    System.out.println("-Get lost!");
                    return;
                }
            }
        }
    }

    public void run() {
        while (1 == 1) {
            displayMenu();
            System.out.println("Menu:");
            Scanner scanner = new Scanner(System.in);
            try {
                int chose = Integer.parseInt(scanner.nextLine());
                switch (chose) {
                    case 1: {
                        displayStats();
                        break;
                    }
                    case 2: {
                        displayInventory();
                        break;
                    }
                    case 3: {
                        ps.Heal();
                        break;
                    }
                    case 4: {
                        mfs.fullFight();
                        ps.checkRank();
                        break;
                    }
                    case 5: {
                        tradeMenu();
                        break;
                    }
                    case 6: {
                        displaySoulCount();
                        break;
                    }
                    case 0: {
                        return;
                    }
                }
            } catch (Exception e) {
                System.out.println("Invalid command!");
            }
        }

    }


}
