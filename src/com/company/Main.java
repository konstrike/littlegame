package com.company;

import com.company.Entities.*;
import com.company.Entities.Items.Item;
import com.company.Entities.Items.Shard;
import com.company.Entities.Mobs.BasicMonster;
import com.company.Entities.Mobs.Monster;
import com.company.Repository.ItemRepo;
import com.company.Services.MobFightService;
import com.company.Services.PlayerService;
import com.company.Services.TradeBuyService;
import com.company.UI.BaseUI;

import java.util.Map;

public class Main {

    public static void main(String[] args) {
        Player player=new Player("Vlad",100);
        //System.out.println(player.toString());

        //System.out.println(monster.toString());

        ItemRepo inventory=new ItemRepo();
        inventory.add(1,10);

        MobFightService mfs=new MobFightService(player,inventory);
        PlayerService ps=new PlayerService(player);
        TradeBuyService tbs=new TradeBuyService(player,inventory);

        //System.out.println(new Random().nextInt(5)+5);

        BaseUI ui=new BaseUI(player,inventory,mfs,ps,tbs);

        ui.run();
    }
}
