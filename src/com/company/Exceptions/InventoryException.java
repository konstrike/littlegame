package com.company.Exceptions;

public class InventoryException extends RuntimeException{
    public InventoryException(String message){
        super(message);
    }
}
