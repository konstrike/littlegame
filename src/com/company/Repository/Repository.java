package com.company.Repository;

import com.company.Entities.Items.Item;
import com.company.Exceptions.InventoryException;

import java.util.List;

public interface Repository {
    void add(Item obj);
    void delete(int id);
    void update(Item obj);
    List<Item> getAll();
    Item getOne(int id) throws InventoryException;
}
