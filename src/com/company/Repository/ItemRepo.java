package com.company.Repository;


import com.company.Entities.Items.Item;
import com.company.Entities.Items.Shard;
import com.company.Exceptions.InventoryException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ItemRepo {

    List<Item> repo;
    Map<Integer, String[]> allItems = new HashMap<>();

    public ItemRepo() {
        repo = new ArrayList<>();
        try {
            populateAllItems();
        }catch (Exception ex)
        {
            System.out.println("Failed to read items >.<");
        }
    }

    //string[0]->name, string[1]->type
    private void populateAllItems() throws IOException {
        File file = new File("E:\\Data files\\Desktop\\Eu\\src\\com\\company\\Entities\\Items\\Items");

        BufferedReader br = new BufferedReader(new FileReader(file));

        String st;
        while ((st = br.readLine()) != null) {
            String[] item = st.split(";");
            String[] arg = {item[1], item[2]};
            allItems.put(Integer.parseInt(item[0]), arg);
        }
    }

    public Map<Integer,String[]> getItems()
    {
        return allItems;
    }

    public String getNameById(int id)
    {
        return allItems.get(id)[0];
    }

    public void add(int id,int quantity) {
        int poz=searchElem(id);
        if(poz!=-1) {
            Item newitem = this.repo.get(poz);
            newitem.increaseQuantity(quantity);
            repo.set(poz,newitem);
        }
        else {
            Item item=null;
            if(allItems.get(id)[1].equals("shard"))
                item=new Shard(id,allItems.get(id)[0],quantity);
            repo.add(item);
        }
    }

    public void delete(int id) {
        repo.remove(id);
    }

    private int searchElem(int id)
    {
        for(int i=0;i<repo.size();i++)
        {
            if(id==repo.get(i).getId())
                return i;
        }
        return -1;

    }

    public void update(Item obj) {
        int poz=searchElem(obj.getId());
        if(poz!=-1)
        {
            repo.set(poz,obj);
        }
    }

    public List<Item> getAll() {
        return repo;
    }

    public Item getOne(int id) throws InventoryException {
        try {
            return repo.get(searchElem(id));
        }catch (Exception e)
        {
            throw new InventoryException("There are no element with such id!");
        }
    }
}
