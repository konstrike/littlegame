package com.company.Services;

import com.company.Entities.Items.Item;
import com.company.Entities.Player;
import com.company.Exceptions.InvalidAmountException;
import com.company.Exceptions.InventoryException;
import com.company.Repository.ItemRepo;

public class TradeBuyService {
    private Player player;
    private ItemRepo inventory;

    public TradeBuyService(Player player, ItemRepo inventory)
    {
        this.player=player;
        this.inventory=inventory;
    }

    public void upgrade(int option) throws Exception {
        switch (option)
        {
            case 1:{
                try {
                    Item item=inventory.getOne(1);
                    if(item.getQuantity()<10)
                    {
                        throw new InvalidAmountException("Not enough red shards!\n");
                    }
                    else
                    {
                        item.increaseQuantity(-10);
                        System.out.println("Shady Merchant:");
                        System.out.println("-I will take good care of your shards.");
                        System.out.println(item.getName()+" left:"+item.getQuantity());
                        System.out.println("Your spiritual power increased!\n");
                        player.setDamage(player.takeDamage()+10);
                    }
                }catch (InventoryException e)
                {
                    throw new Exception("No such item!");
                }
                break;
            }
            case 2:{
                try {
                    Item item=inventory.getOne(2);
                    if(item.getQuantity()<10)
                    {
                        throw new InvalidAmountException("Not enough blue shards!\n");
                    }
                    else
                    {
                        item.increaseQuantity(-10);
                        System.out.println("Shady Merchant:");
                        System.out.println("-I will take good care of your shards.");
                        System.out.println(item.getName()+" left:"+item.getQuantity());
                        System.out.println("Your HP increased!\n");
                        player.setMaxHp(player.getMaxHp()+10);
                    }
                }catch (InventoryException e)
                {
                    throw new Exception("No such item!");
                }
                break;
            }
            case 0:{
                break;
            }
        }
    }
}
