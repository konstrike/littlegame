package com.company.Services;

import com.company.Entities.Mobs.BasicMonster;
import com.company.Entities.Mobs.Monster;
import com.company.Entities.Player;
import com.company.Repository.ItemRepo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.Buffer;
import java.util.*;

public class MobFightService {
    private Player player;
    private Monster monster;
    private ItemRepo inventory;
    private int stage = 1;
    private int level = 1;
    private List<Monster> monsters = new ArrayList<>();

    public MobFightService(Player p, ItemRepo inventory) {
        this.player = p;
        this.inventory = inventory;
        initializeLevel();
    }

    private void initializeLevel() {
        this.monsters.clear();
        File levelsFile = new File("E:\\Data files\\Desktop\\Eu\\src\\com\\company\\Services\\Levels");
        Map<Integer, Integer> levelinfo = new LinkedHashMap<>();
        try {
            BufferedReader leverReder = new BufferedReader(new FileReader(levelsFile));
            String lvlread;

            //levelinfo is a map with monster id and how many of them are

            while ((lvlread = leverReder.readLine()) != null) {
                String[] textLevel = lvlread.split(";");
                //textLevel[0] is actual level
                if (Integer.parseInt(textLevel[0]) == this.level) {
                    int nr = 1;
                    try {
                        //add to levelinfo map the id and number
                        while (textLevel[nr] != null) {
                            levelinfo.put(Integer.parseInt(textLevel[nr]), Integer.parseInt(textLevel[nr + 1]));
                            nr += 2;
                        }
                    } catch (Exception e) {
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Failed to read level information");
        }

        File file = new File("E:\\Data files\\Desktop\\Eu\\src\\com\\company\\Repository\\Mobs");
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));

            String st;
            while ((st = br.readLine()) != null) {
                String[] item = st.split(";");
                //check levelinfo to see if it needs to add the monster to monsters list
                for (int i : levelinfo.keySet())
                    if (Integer.parseInt(item[0]) == i) {
                        int count = 0;
                        while (count < levelinfo.get(i)) {
                            monsters.add(new BasicMonster(item[1], Integer.parseInt(item[2]),
                                    Integer.parseInt(item[3]), Integer.parseInt(item[4]), item[5],
                                    Integer.parseInt(item[6]), Integer.parseInt(item[7])));
                            count++;
                        }
                    }
            }
        } catch (Exception e) {
            System.out.println("Failed to initialize level");
        }
    }


    private void displayFightDetails() {
        for (int i = 0; i < 20 + player.getName().length() + monster.getName().length(); i++) {
            System.out.print("-");
        }
        System.out.print("\n" + player.getName());
        for (int i = 0; i < 20 - player.getName().length(); i++) {
            System.out.print(" ");
        }
        System.out.print(monster.getName());
        System.out.print("\n" + player.getHp());
        for (int i = 0; i < 20 - String.valueOf(player.getHp()).length(); i++) {
            System.out.print(" ");
        }
        System.out.print(monster.getHP() + "\n");
        for (int i = 0; i < 20 + player.getName().length() + monster.getName().length(); i++) {
            System.out.print("-");
        }
        System.out.println("");
    }


    public void fullFight() {
        if (this.stage == 11) {
            this.level++;
            this.stage = 1;
            initializeLevel();
        }

        System.out.println("You are currently at: \nLevel:" + this.level + "\nStage:" + this.stage + "\n");
        monster = monsters.get(this.stage - 1);

        //Fighting part
        System.out.println("Starting the fight:");
        while (player.getHp() > 0 && monster.getHP() > 0) {
            int playerDmg10 = player.takeDamage() - player.takeDamage() / 10;
            int monsterDmg10 = monster.getDmg() - monster.getDmg() / 10;
            int actuallyPlayerDmg = new Random().nextInt((player.takeDamage() - playerDmg10) + 1) + playerDmg10;
            int actuallyMonsterDmg = new Random().nextInt((monster.getDmg() - monsterDmg10) + 1) + monsterDmg10;
            displayFightDetails();
            System.out.println("You dealt " + actuallyPlayerDmg + " damage!");
            System.out.println("You received " + actuallyMonsterDmg + " damage");
            monster.takeDamage(actuallyPlayerDmg);
            if (monster.getHP() >= 0)
                player.takeDamage(actuallyMonsterDmg);
        }
        displayFightDetails();


        //Check if the player died or not
        //If the player died, his spiritual power decrease
        if (player.getHp() <= 0) {
            System.out.println("You died!");
            player.setSpower(player.getSPower() - player.getSPower() / 10);
            System.out.println("Your spiritual power slightly decreased!");
        } else {
            //If the player defeted the monster has to chose what to do next
            System.out.println("You won!");
            System.out.println("1)Eat\n2)Spare");
            Scanner scanner = new Scanner(System.in);
            boolean check = false;
            int chose;
            while (check == false) {
                try {
                    chose = Integer.parseInt(scanner.nextLine());
                    check = true;
                    if (chose == 1) {
                        //update the KillCount
                        player.addKill(monster.getRank());

                        //update player spiritual power
                        player.setSpower(player.getSPower() + monster.getSpower());

                        System.out.println("You have eaten " + monster.getName());
                        System.out.println("Your spiritual power increased!");
                        System.out.println("You got:" + monster.getDrop().getQuantity() +
                                " " + inventory.getNameById(monster.getDrop().getItemId()));

                        //get monster drop and add it to player inventory
                        inventory.add(monster.getDrop().getItemId(), monster.getDrop().getQuantity());

                        //increase stage
                        this.stage++;

                    } else {
                        if (chose == 2) {
                            //update the KillCount
                            player.addKill(monster.getRank());

                            //update player spiritual power with half the power
                            player.setSpower(player.getSPower() + monster.getSpower() / 2);

                            System.out.println("You have spared " + monster.getName());
                            System.out.println("Your spiritual power slightly increased!");
                            System.out.println("You got:" + monster.getDrop().getQuantity() +
                                    " " + inventory.getNameById(monster.getDrop().getItemId()));

                            //get monster drop and add it to player inventory
                            inventory.add(monster.getDrop().getItemId(), monster.getDrop().getQuantity());

                            //increase stage
                            this.stage++;
                        } else {
                            throw new Exception("invalid");
                        }
                    }
                } catch (Exception e) {
                    System.out.println("Invalid command!");
                }
            }
        }
    }
}
