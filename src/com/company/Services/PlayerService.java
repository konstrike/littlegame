package com.company.Services;

import com.company.Entities.Player;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;


public class PlayerService {
    private Player player;
    private Map<Integer[],String> allRanks=new LinkedHashMap<>();

    public PlayerService(Player p)
    {

        this.player =p;
        try {
            initRanks();
        } catch (Exception e)
        {
            System.out.println("Error in reading Ranks!");
        }

    }

    public void Heal()
    {
        this.player.setHp(player.getMaxHp());
        System.out.println("You'we been healed!");
    }

    private void initRanks() throws IOException {
        File file = new File("E:\\Data files\\Desktop\\Eu\\src\\com\\company\\Services\\Ranks");

        BufferedReader br = new BufferedReader(new FileReader(file));

        String st;
        while ((st = br.readLine()) != null) {
            String[] rank = st.split(";");
            Integer[] arg = {Integer.parseInt(rank[0]), Integer.parseInt(rank[1])};
            allRanks.put(arg,rank[2]);
        }
    }

    public void checkRank()
    {
        for(Integer[] bound:allRanks.keySet())
        {
            if(bound[0]<player.getSPower() && bound[1]>player.getSPower())
            {
                player.setRank(allRanks.get(bound));
            }
        }
    }
}
