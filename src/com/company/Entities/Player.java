package com.company.Entities;

import com.company.Entities.Items.Item;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Player {
    private String name;

    private int hp;
    private int maxHp;

    private int baseDamage=10;
    private int damage;

    private int spower;
    private String rank;
    private Map<String,Integer> soulCount;



    public Player(String name, int hp)
    {
        this.name=name;
        this.hp=hp;
        this.maxHp=hp;
        this.damage=baseDamage;
        initiate();
    }

    private void initiate()
    {
        soulCount=new LinkedHashMap<>();
        this.soulCount.put("Soul",0);
        this.soulCount.put("Hollow",0);
        this.soulCount.put("Shinigami",0);
        this.soulCount.put("Menos Grande",0);
        this.soulCount.put("Adjuchas",0);
        this.soulCount.put("Vasto Lorde",0);
        this.soulCount.put("Espada",0);
        this.soulCount.put("Shinigami Captain",0);
        rank="Demi-Hollow";
        spower=10;
    }

    public void addKill(String rank)
    {
        for(String ranks:soulCount.keySet())
        {
            if(ranks.equals(rank))
            {
                soulCount.put(rank,soulCount.get(rank)+1);
            }
        }
    }

    public Map<String,Integer> getSoulCount() {
        return soulCount;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int takeDamage() { return this.damage; }

    public void setDamage(int value) {this.damage=value;}

    public int getSPower() {return spower;}

    public void setSpower(int value) {spower=value;}

    public String getRank() {return rank;}

    public void setRank(String rank) {this.rank=rank;}

    public int getMaxHp() {return maxHp;}

    public void setMaxHp(int value) {this.maxHp=value;}


    public void takeDamage(int nr){this.hp-=nr;}


    public String toString()
    {
        return "Name:" + name +
                "\nRank:" + rank+
                "\nHp:" + hp +"/"+maxHp+
                "\nDamage:" + damage +
                "\nSpiritual Power:" + spower;
    }
}
