package com.company.Entities.Mobs;

public class BasicMonster implements Monster {

    private String name;
    private int HP;
    private int Dmg;
    private int Spower;
    private String Rank;
    private Drop drop;


    public BasicMonster()
    {
        this.name="None";
        this.HP=1;
        this.Dmg=1;
        this.Spower=1;
        this.Rank="Small Hollow";
        this.drop=new Drop();
    }

    public BasicMonster(String name,int HP,int Dmg,int Spower,String Rank,int itemId,int quantity)
    {
        this.name=name;
        this.HP=HP;
        this.Dmg=Dmg;
        this.Spower=Spower;
        this.Rank=Rank;
        this.drop=new Drop(itemId,quantity);
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getHP() {
        return this.HP;
    }

    @Override
    public int getDmg() {
        return this.Dmg;
    }

    public String getRank() { return Rank; }

    public void setRank(String rank) { Rank = rank; }

    public int getSpower() { return Spower; }

    public void setSpower(int spower) { Spower = spower; }

    public Drop getDrop() {return drop;}

    public void setDrop(Drop drop) {this.drop=drop;}


    public void takeDamage(int nr) { this.HP -= nr; }


    public String toString() {
        return "Name:" + name +
                "\nRank:" + Rank+
                "\nHp:" + HP +
                "\nDamage:" + Dmg +
                "\nSpiritual Power:" + Spower;
    }
}
