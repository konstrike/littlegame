package com.company.Entities.Mobs;

public interface Monster {
    String getName();
    int getHP();
    int getDmg();
    int getSpower();
    String getRank();
    void takeDamage(int nr);
    Drop getDrop();

    String toString();
}
