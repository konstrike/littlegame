package com.company.Entities.Mobs;

public class Drop {
    private int itemId;
    private int quantity;

    public Drop() {}

    public Drop(int itemId,int quantity)
    {
        this.itemId=itemId;
        this.quantity=quantity;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String toString()
    {
        return "Drop:"+itemId+
                "\nQuantity:"+quantity;
    }
}
