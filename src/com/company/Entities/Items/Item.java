package com.company.Entities.Items;

public abstract class Item {
    public abstract int getId();
    public abstract String getName();
    public abstract String getType();
    public abstract int getQuantity();
    public abstract void increaseQuantity(int nr);
    public String toString(){
        return "Item";
    }

}
