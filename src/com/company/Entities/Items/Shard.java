package com.company.Entities.Items;

public class Shard extends Item{

    private int id;
    private String name;
    private String type;
    private int quantity;

    public Shard(int id,String name) {
        this.id=id;
        this.name=name;
        this.type="Shard";
        this.quantity=1;
    }

    public Shard(int id,String name,int quantity) {
        this.id=id;
        this.name=name;
        this.type="Shard";
        this.quantity=quantity;
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public int getQuantity() {
        return this.quantity;
    }

    @Override
    public void increaseQuantity(int nr) {
        this.quantity+=nr;
    }


    @Override
    public String toString() {
        return "Name:"+this.name+
                "\nType:"+this.type+
                "\nQuantity:"+this.quantity;
    }
}
