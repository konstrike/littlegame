package Tests;


import com.company.Entities.Player;

import static org.junit.Assert.*;

public class PlayerTest {

    private Player player;

    @org.junit.Before
    public void setUp(){
        player=new Player("Vlad",10);
    }

    @org.junit.Test
    public void getName() {
        assertEquals("Vlad",player.getName());
    }

    @org.junit.Test
    public void setName() {
        Player player1=new Player("vlad",10);
        player1.setName("Alex");
        assertEquals("Alex",player1.getName());
    }

    @org.junit.Test
    public void getHp() {
        assertEquals(10,player.getHp());
    }

    @org.junit.Test
    public void setHp() {
        Player player1=new Player("vlad",10);
        player1.setHp(11);
        assertEquals(11,player1.getHp());
    }

    @org.junit.Test
    public void getDamage() {
        assertEquals(10,player.takeDamage());
    }

    @org.junit.Test
    public void setDamage() {
        Player player1=new Player("vlad",10);
        player1.setDamage(11);
        assertEquals(11,player1.takeDamage());
    }

    @org.junit.Test
    public void getSPower() {
        assertEquals(10,player.getSPower());
    }

    @org.junit.Test
    public void setSpower() {
        Player player1=new Player("vlad",10);
        player1.setSpower(11);
        assertEquals(11,player1.getSPower());
    }

    @org.junit.Test
    public void getRank() {
        assertEquals("Small Hollow",player.getRank());
    }

    @org.junit.Test
    public void setRank() {
        Player player1=new Player("vlad",10);
        player1.setRank("Medium Hollow");
        assertEquals("Medium Hollow",player1.getRank());
    }

    @org.junit.Test
    public void getMaxHp() {
        assertEquals(10,player.getMaxHp());
    }

    @org.junit.Test
    public void setMaxHp() {
        Player player1=new Player("vlad",10);
        player1.setMaxHp(11);
        assertEquals(11,player1.getMaxHp());
    }

    @org.junit.Test
    public void inflictDamage() {
        Player player1=new Player("vlad",10);
        player1.takeDamage(3);
        assertEquals(7,player1.getHp());
    }

//    @org.junit.Test
//    public void toString() {
//    }
}
